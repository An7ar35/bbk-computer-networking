import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

/**
 * <b>Client</b>
 * 
 * @author Es A. DAVISON
 * @version 1.0
 */
class Server {
    private static int packetCountIn = 0;
    private static int packetCountOut = 0;
    private static final int port = 1999;
    private static int packetPort;
    private static InetAddress packetAddress;

    public static void main( String args[] ) throws Exception {
        packetAddress = InetAddress.getLocalHost();
        DatagramSocket serverSocket = new DatagramSocket( port );
        System.out.println( "[ SERVER | Listening on port " + serverSocket.getLocalPort() + " ]" );
        int num;
        byte[] receivedData = new byte[ 4 ];
        byte[] sendData = new byte[ 4 ];

        while ( true ) {
            // receive
            receivePacket( serverSocket, receivedData );
            num = ByteBuffer.wrap( receivedData ).getInt();
            num -= 2;
            // send
            sendData = ByteBuffer.allocate( 4 ).putInt( num ).array();
            sendPacket( serverSocket, sendData );
        }
    }

    /**
     * receivePacket
     * 
     * @param clientSocket
     *            DatagramSocket
     * @param receivedData
     *            byte array for received data
     */
    private static void receivePacket( DatagramSocket serverSocket, byte[] receivedData ) {
        try {
            DatagramPacket receivedPacket = new DatagramPacket( receivedData, receivedData.length );
            serverSocket.receive( receivedPacket );
            packetAddress = receivedPacket.getAddress();
            packetPort = receivedPacket.getPort();
            packetCountIn++;
            System.out.println( "[ Packet.in [#" + packetCountIn + "] | " + packetAddress + ":" + packetPort + " | Content: <" + ByteBuffer.wrap( receivedData ).getInt() + "> ]" );
        } catch ( IOException e ) {
            System.out.println( "IO error in recieving" );
        }
    }

    /**
     * sendPacket
     * 
     * @param clientSocket
     *            DatagramSocket
     * @param sendData
     *            byte array to send
     */
    private static void sendPacket( DatagramSocket serverSocket, byte[] sendData ) {
        try {
            DatagramPacket sendPacket = new DatagramPacket( sendData, sendData.length, packetAddress, packetPort );
            serverSocket.send( sendPacket );
            packetCountOut++;
            System.out.println( "[ Packet.out [#" + packetCountOut + "] | " + packetAddress + ":" + packetPort + " | Content: <" + ByteBuffer.wrap( sendData ).getInt() + "> ]" );
        } catch ( IOException e ) {
            System.out.println( "IO error in sending" );
        }
    }
}