import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.Scanner;

/**
 * <b>Client</b>
 * 
 * @author Es A. DAVISON
 * @version 1.0
 */
class Client {
    private static int packetCountIn = 0;
    private static int packetCountOut = 0;
    private static final int port = 1999;
    private static InetAddress address;
    private static final int maxRetry = 5;

    public static void main( String args[] ) throws Exception {
        address = InetAddress.getLocalHost();
        DatagramSocket clientSocket = new DatagramSocket();
        clientSocket.setSoTimeout( 500 );
        int retryCount = 0;
        System.out.println( "[ CLIENT | " + address + ":" + port + " ]" );
        boolean flag = false; // Run control flag
        byte[] sendData = new byte[ 4 ];
        byte[] receivedData = new byte[ 4 ];
        int num = getInput();

        while ( flag == false ) {
            // Sending
            sendData = ByteBuffer.allocate( 4 ).putInt( num ).array();
            boolean success = sendPacket( clientSocket, sendData );           
            if ( success == false ) {
                while ( success == false && retryCount < maxRetry ) {
                    retryCount++;
                    System.out.println( "Retrying to send " + retryCount + "/" + maxRetry );
                    success = sendPacket( clientSocket, receivedData );   
                }
                if ( success == false && retryCount == maxRetry ) {
                    System.out.println( "Failure to send to server. Aborting." );
                    flag = true;
                }
            }

            if ( flag == false ) {
                retryCount = 0;
                // Receiving
                success = receivePacket( clientSocket, receivedData );
                if ( success == false ) {
                    while ( success == false && retryCount < maxRetry ) {
                        retryCount++;
                        System.out.println( "Retrying to receive " + retryCount + "/" + maxRetry );
                        success = receivePacket( clientSocket, sendData );
                    }
                    if ( success == false && retryCount == maxRetry ) {
                        System.out.println( "Failure to send to server. Aborting." );
                        flag = true;
                    }
                }
            }

            if ( flag == false ) {
                num = ByteBuffer.wrap( receivedData ).getInt();
                retryCount = 0;
                // Checking num
                if ( num > 0 ) {
                    num -= 2;
                } else {
                    flag = true;
                    System.out.println( "-- Finished --" );
                }
            }
        }
        clientSocket.close();
    }

    /**
     * sendPacket
     * 
     * @param clientSocket
     *            DatagramSocket
     * @param sendData
     *            byte array to send
     * @return whether or not the sending of the packet was successful as a
     *         boolean value
     */
    private static boolean sendPacket( DatagramSocket clientSocket, byte[] sendData ) {
        try {
            DatagramPacket sendPacket = new DatagramPacket( sendData, sendData.length, address, port );
            clientSocket.send( sendPacket );
            packetCountOut++;
            System.out.println( "[ Packet.out [#" + packetCountOut + "] | " + address + ":" + port + " | Content: <" + ByteBuffer.wrap( sendData ).getInt() + "> ]" );
            return true;
        } catch ( IOException e ) {
            System.out.println( "IO error in sending" );
            return false;
        }
    }

    /**
     * receivePacket
     * 
     * @param clientSocket
     *            DatagramSocket
     * @param receivedData
     *            byte array for received data
     * @return whether or not the receipt of the packet was successful as a
     *         boolean value
     */
    private static boolean receivePacket( DatagramSocket clientSocket, byte[] receivedData ) {
        try {
            DatagramPacket receivedPacket = new DatagramPacket( receivedData, receivedData.length );
            clientSocket.receive( receivedPacket );
            packetCountIn++;
            System.out.println( "[ Packet.in [#" + packetCountIn + "] | " + address + ":" + port + " | Content: <" + ByteBuffer.wrap( receivedData ).getInt() + "> ]" );
            return true;
        } catch ( SocketTimeoutException e ) {
            System.out.println( "No response from server..." );
            return false;
        } catch ( IOException e ) {
            System.out.println( "IO error in recieving" );
            return false;
        }
    }
    
    /**
     * getInput
     * 
     * @return the user input integer
     */
    public static int getInput() {
        Scanner scanner = new Scanner( System.in );
        System.out.print( "Enter integer: " );
        while ( true ) {
            if ( scanner.hasNextInt() ) {
                int input = scanner.nextInt();
                scanner.nextLine();
                scanner.close();
                return input;
            } else {
                System.out.print( "\"" + scanner.nextLine() + "\" not a valid integer. Enter integer: " );
            }
        }
    }
}
